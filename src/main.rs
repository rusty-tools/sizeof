use std::path::Path;
use std::env::args;
use std::io::stdout;
use std::io::Write;

static KIB: f64 = 1024.0;
static MIB: f64 = 1024.0 * KIB;
static GIB: f64 = 1024.0 * MIB;
static TIB: f64 = 1024.0 * GIB;

use walkdir::WalkDir;
const VERSION: &str = env!( "CARGO_PKG_VERSION" );

fn weigh_node( path: &Path ) -> i128 {

	let mut size: i128 = 0;

	let walker = WalkDir::new( path )
            .follow_links( false )
            .same_file_system( true );

    let path_meta = match path.symlink_metadata() {
        Ok( path_meta ) => path_meta,
        Err( _err ) => {
            return -3;
        }
    };

    /* Check if we have a symlink */
    if path_meta.is_symlink() {
        return -1;
    }

    /* Check if we have anything other than file/folder */
    if !path_meta.is_file() && !path_meta.is_dir() {
        return -2;
    }

    for result in walker {
        let dent = match result {
            Ok(dent) => dent,
            Err(_err) => {
                continue;
            }
        };

        if !dent.file_type().is_file() {
            continue;
        }

        let meta = match dent.metadata() {
            Ok(meta) => meta,
            Err(_err) => {
                continue;
            }
        };

        size += meta.len() as i128;
    }

    return size;
}

fn format_size( size: i128, fixedformat: bool ) -> String {

	if size == i128::max_value() {
		return format!( "             -- Unreadable location" );
	}

	let size = size as f64;

	// Format is fixed: User chose it
	if fixedformat {
		let fmt = args().nth( 1 ).unwrap();
		match fmt.as_ref() {
			"-b" => { return format!( "{:>15.00} B", size ) },							// Bytes
			"-k" => { return format!( "{:>15.02} KiB", size / KIB ) },					// KiB
			"-m" => { return format!( "{:>15.02} MiB", size / MIB ) },					// MiB
			"-g" => { return format!( "{:>15.02} GiB", size / GIB ) },					// GiB
			_    => { return format!( "{:>15.02} TiB", size / TIB ) },					// TiB and anything else
		}
	}

	// Format is not fixed: Decide the size based on the actual size
	else {
		if      size >= TIB { return format!( "{:>15.02} TiB", size / TIB ) }
		else if size >= GIB { return format!( "{:>15.02} GiB", size / GIB ) }
		else if size >= MIB { return format!( "{:>15.02} MiB", size / MIB ) }
		else if size >= KIB { return format!( "{:>15.02} KiB", size / KIB ) }
		else                { return format!( "{:>12}    B", size ) };
	}
}

fn usage() {

	println!( "sizeof v{}", VERSION );
	println!( "Usage : sizeof {{-b|-k|-m|-g|-t}} dir1 dir2 ..." );
}

fn main() {

	let argc = args().len();

	match argc {													// switch statement of C
		1 => usage(),												// If there is only the executalbe name; print usage

		2 => {																			// Two arguments
			let arg = args().nth( 1 ).expect( "Unable to access the argument!" );		// Get the 1st argument
			if arg.starts_with( "-" ) {													// If its a format specifier,
				usage();																// Print the usage
			}

			else {
				println!( "sizeof v{}\n", VERSION );
				println!( "=============================================" );

				let arg = args().nth( 1 ).expect( "Unable to access the argument!" );

                print!( "{:0width$}    ",
                    &arg.as_str(),
                    width = *&arg.len() as usize
                );

                stdout().flush().unwrap();

                let node_weight: i128 = weigh_node( Path::new( &arg ) );

				match node_weight {
					-1 => {
						println!( "{}     -> symlink", format_size( 0, false ).to_string() );
					},

					-2 => {
						println!( "{}     -> not a file/folder", format_size( 0, false ).to_string() );
					},

					-3 => {
						println!( "{}     -> inaccessible location", format_size( 0, false ).to_string() );
					},

					_  => {
						println!( "{}", format_size( node_weight, false ).to_string() );
					}
				}
			}
		},

		_ => {
			let arg = args().nth( 1 ).expect( "Unable to access the argument at 1" );

			let mut maxwidth: u32 = 0;

			let fixedformat: bool = if arg.starts_with( "-" ) { true } else { false };
			let start: usize = if fixedformat { 2 } else { 1 };

			println!( "sizeof v{}\n", VERSION );
			println!( "=============================================" );

			for i in start..argc {
				let arg = args().nth( i ).expect( "Unable to access the argument" );
				maxwidth = if *&arg.len() as u32 > maxwidth { *&arg.len() as u32 } else { maxwidth };
			}

			for i in start..argc {
				let arg = args().nth( i ).expect( "Unable to access the argument!" );

                print!( "{:0width$}    ",
                    &arg.as_str(),
                    width = maxwidth as usize
                );

                stdout().flush().unwrap();

				let node_weight: i128 = weigh_node( Path::new( &arg ) );

				match node_weight {
					-1 => {
						println!( "{}     -> symlink", format_size( 0, fixedformat ).to_string() );
					},

					-2 => {
						println!( "{}     -> not a file/folder", format_size( 0, fixedformat ).to_string() );
					},

					-3 => {
						println!( "{}     -> inaccessible location", format_size( 0, fixedformat ).to_string() );
					},

					_  => {
						println!( "{}", format_size( node_weight, fixedformat ).to_string() );
					}
				}
			}
		}
	}
}
